package Mitternachtsformel;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main extends Application {
    Stage stage;
    Scene[] scene;
    SolverViewInterface[] pane;
    Scene templateSelectViewScene;
    TemplateSelectView templateSelectView;

    @Override
    public void start(Stage stage) {
        this.stage = stage;
        this.pane = new SolverViewInterface[3];
        this.templateSelectView = new TemplateSelectView();
        this.templateSelectViewScene = new Scene(templateSelectView, 360, 210);
        this.pane[1] = new SolverView3();
        this.pane[2] = new SolverView2();
        try {
            ((SolverView2) this.pane[2]).setView(FXMLLoader.load(getClass().getResource("solverview2.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.pane[0] = new SolverView();

        this.scene = new Scene[3];
        for (int i = 0; i < this.pane.length; i++) {
            if (this.pane[i] instanceof SolverView2) {
                this.scene[i] = new Scene(((SolverView2) this.pane[i]).getView());
            } else
                this.scene[i] = new Scene((Parent) this.pane[i], 360, 210);
        }
        this.stage.setTitle("Mitternachtsformel");
        int aktIndex = getPersistedIndex();
        if (aktIndex != -1) {
            this.changeTheme(aktIndex);
        } else {
            this.stage.setScene(this.templateSelectViewScene);
            this.setChangeThemeMenuItemsEvents();
            this.stage.show();
        }
    }

    private void setChangeThemeMenuItemsEvents() {
        MenuItem[] changeThemeMenuItems = this.templateSelectView.getChangeThemeMenuItems();
        for (MenuItem theme : changeThemeMenuItems) {
            theme.setOnAction(e -> {
                MenuItem selected = (MenuItem) e.getSource();
                changeTheme((Integer) selected.getUserData());
            });
        }
    }

    public void changeTheme(int aktIndex) {
        this.stage.setScene(this.scene[aktIndex]);
        this.stage.show();
        new SolverController(this.pane[aktIndex]);

        MenuItem changeTheme = this.pane[aktIndex].getChangeThemeMenuItem();
        if (changeTheme != null) {
            changeTheme.setOnAction(e -> {
                this.stage.setScene(this.templateSelectViewScene);
                this.setChangeThemeMenuItemsEvents();
                this.stage.show();
            });
        } else {
            System.out.println("ERROR: No MenuItem found! for view " + aktIndex);
        }

        persistIndex(aktIndex);
    }

    private void persistIndex(int index) {
        try (FileWriter configFile = new FileWriter("config.ini")) {
            configFile.write("TemplateViewIndex=\"" + index + "\"");
        } catch (IOException ignored) {

        }
    }

    private int getPersistedIndex() {
        int index = -1;
        try (BufferedReader configFile = new BufferedReader(new FileReader("config.ini"))) {
            String line = configFile.readLine();
            if (line.contains("TemplateViewIndex=\"")) {
                index = Integer.parseInt(line.substring(19, 20));
            }
        } catch (IOException ignored) {

        }
        return index;
    }
}
