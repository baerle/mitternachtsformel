package Mitternachtsformel;

public class Mitternachtsformel {
    public static double[] calcZeroPoint(int a, int b, int c) {
        double[] result = new double[2];

        result[0] = (-b + Math.sqrt(b * b - 4 * a * c)) / 2 * a;
        result[1] = (-b - Math.sqrt(b * b - 4 * a * c)) / 2 * a;

        return result;
    }
}
