package Mitternachtsformel;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class SolverController {
    private final SolverViewInterface view;

    public SolverController(SolverViewInterface view) {
        this.view = view;

        run();
    }

    private void run() {
        Button submitButton = this.view.getSubmitButton();
        if (submitButton != null) {
            submitButton.setOnAction(e -> calculateMitternachtsformel());
        }
    }

    @FXML
    private void calculateMitternachtsformel() {
        int a, b, c;
        try {
            a = parseInputToInt(this.view.getContentFromA());
            b = parseInputToInt(this.view.getContentFromB());
            c = parseInputToInt(this.view.getContentFromC());

            double[] result = Mitternachtsformel.calcZeroPoint(a, b, c);

            this.view.setTextOnResultLabel("1. Result: " + result[0] + "     2. Result: " + result[1]);
        } catch (NumberFormatException nfe) {
            this.view.setTextOnResultLabel("Wrong Input types: Must be numbers");
        }
    }

    private int parseInputToInt(String input) {
        if ((input = input.trim()).equals(""))
            return 0;
        return Integer.parseInt(input);
    }
}
