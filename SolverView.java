package Mitternachtsformel;

import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class SolverView extends VBox implements SolverViewInterface {
    private final TextField a, b, c;
    private final Button calc;
    private final Label result;
    MenuBar menubar;
    Menu menu;
    MenuItem menuitem;
    int[] data;

    public SolverView() {
        super.setSpacing(5);

        this.menuitem = new MenuItem("Change Theme");
        this.menu = new Menu("Settings");
        this.menu.getItems().addAll(this.menuitem);
        this.menubar = new MenuBar(this.menu);

        this.a = new TextField();
        this.b = new TextField();
        this.c = new TextField();

        this.a.setPromptText("a");
        this.b.setPromptText("b");
        this.c.setPromptText("c");

        HBox hbox = new HBox(2, this.a, this.b, this.c);
        hbox.setSpacing(5);

        this.calc = new Button("Calculate");

        this.result = new Label();

        super.getChildren().addAll(this.menubar, hbox, this.calc, this.result);

        this.data = new int[3];

        this.a.setOnKeyTyped(event -> {
            if (event.getCharacter().equals("\r") || event.getCharacter().equals("\n")) {
                this.calc.fire();
            }
        });

        this.b.setOnKeyTyped(event -> {
            if (event.getCharacter().equals("\r") || event.getCharacter().equals("\n")) {
                this.calc.fire();
            }
        });

        this.c.setOnKeyTyped(event -> {
            if (event.getCharacter().equals("\r") || event.getCharacter().equals("\n")) {
                this.calc.fire();
            }
        });
    }

    @Override
    public String getContentFromA() {
        return this.a.getText();
    }

    @Override
    public String getContentFromB() {
        return this.b.getText();
    }

    @Override
    public String getContentFromC() {
        return this.c.getText();
    }

    @Override
    public void setTextOnResultLabel(String text) {
        this.result.setText(text);
    }

    @Override
    public Button getSubmitButton() {
        return this.calc;
    }

    @Override
    public MenuItem getChangeThemeMenuItem() {
        return this.menuitem;
    }
}
