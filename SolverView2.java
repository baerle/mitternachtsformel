package Mitternachtsformel;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;

public class SolverView2 extends Parent implements SolverViewInterface {
    private Parent view;
    int[] data;

    @FXML
    TextField a, b, c;
    @FXML
    Button submit;
    @FXML
    Label question, labelA, labelB, result1, result2;
    @FXML
    MenuItem menuitem;
    @FXML
    Menu menu;
    @FXML
    MenuBar menubar;

    private MenuItem menuItem;

    public SolverView2() {

    }

    @FXML
    public void initialize() {
        this.menuItem = this.menuitem;
        this.a.setOnKeyTyped(event -> {
            if (event.getCharacter().equals("\r") || event.getCharacter().equals("\n")) {
                this.submit.fire();
            }
        });

        this.b.setOnKeyTyped(event -> {
            if (event.getCharacter().equals("\r") || event.getCharacter().equals("\n")) {
                this.submit.fire();
            }
        });

        this.c.setOnKeyTyped(event -> {
            if (event.getCharacter().equals("\r") || event.getCharacter().equals("\n")) {
                this.submit.fire();
            }
        });
    }

    public void setView(Parent view) {
        this.view = view;
    }

    public Parent getView() {
        return view;
    }

    @Override
    public String getContentFromA() {
        return this.a.getText();
    }

    @Override
    public String getContentFromB() {
        return this.b.getText();
    }

    @Override
    public String getContentFromC() {
        return this.c.getText();
    }

    @Override
    public void setTextOnResultLabel(String text) {
        String result1 = text.substring(0, text.length() / 2 - 1).trim();
        String result2 = text.substring(text.length() / 2).trim();

        this.result1.setText(result1);
        this.result2.setText(result2);
    }

    @Override
    public Button getSubmitButton() {
        return this.submit;
    }

    @Override
    @FXML
    public MenuItem getChangeThemeMenuItem() {
        if (this.menuitem == null) {
            if (this.menu == null) {
                if (this.menubar == null)
                    System.out.println("No menu available!");
                else
                    return this.menubar.getMenus().get(0).getItems().get(0);
            } else {
                return this.menu.getItems().get(0);
            }
        }
        if (this.menuItem == null)
            return this.menuitem;
        return this.menuItem;
    }
}
