package Mitternachtsformel;

import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class SolverView3 extends VBox implements SolverViewInterface {
    MenuBar menubar;
    Menu menu;
    MenuItem menuitem;
    TextField a, b, c;
    Button submit;
    Label question, labelA, labelB, result1, result2;

    public SolverView3() {
        super.setSpacing(5);

        this.menuitem = new MenuItem("Change Theme");
        this.menu = new Menu("Settings");
        this.menu.getItems().addAll(this.menuitem);
        this.menubar = new MenuBar(this.menu);

        this.question = new Label("Inset your function:");

        this.a = new TextField();
        this.a.setPromptText("a");
        this.a.setPrefWidth(30);
        this.labelA = new Label(" x² + ");
        this.b = new TextField();
        this.b.setPromptText("b");
        this.b.setPrefWidth(30);
        this.labelB = new Label(" x + ");
        this.c = new TextField();
        this.c.setPromptText("c");
        this.c.setPrefWidth(30);
        HBox hbox = new HBox(this.a, this.labelA, this.b, this.labelB, this.c);

        this.submit = new Button("Calculate");

        this.result1 = new Label();
        this.result2 = new Label();

        super.getChildren().addAll(this.menubar, this.question, hbox, this.submit, this.result1, this.result2);

        this.a.setOnKeyTyped(event -> {
            if (event.getCharacter().equals("\r") || event.getCharacter().equals("\n")) {
                this.submit.fire();
            }
        });

        this.b.setOnKeyTyped(event -> {
            if (event.getCharacter().equals("\r") || event.getCharacter().equals("\n")) {
                this.submit.fire();
            }
        });

        this.c.setOnKeyTyped(event -> {
            if (event.getCharacter().equals("\r") || event.getCharacter().equals("\n")) {
                this.submit.fire();
            }
        });
    }

    @Override
    public String getContentFromA() {
        return this.a.getText();
    }

    @Override
    public String getContentFromB() {
        return this.b.getText();
    }

    @Override
    public String getContentFromC() {
        return this.c.getText();
    }

    @Override
    public void setTextOnResultLabel(String text) {
        String result1 = text.substring(0, text.length() / 2 - 1).trim();
        String result2 = text.substring(text.length() / 2).trim();

        this.result1.setText(result1);
        this.result2.setText(result2);
    }

    @Override
    public Button getSubmitButton() {
        return this.submit;
    }

    @Override
    public MenuItem getChangeThemeMenuItem() {
        return this.menuitem;
    }
}
