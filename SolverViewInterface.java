package Mitternachtsformel;

import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;

public interface SolverViewInterface {
    String getContentFromA();

    String getContentFromB();

    String getContentFromC();

    void setTextOnResultLabel(String text);

    Button getSubmitButton();

    MenuItem getChangeThemeMenuItem();
}
