package Mitternachtsformel;

import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.StackPane;

public class TemplateSelectView extends StackPane {
    MenuButton menuButton;
    MenuItem template0, template1, template2;

    public TemplateSelectView() {
        this.template0 = new MenuItem("Template0");
        this.template0.setUserData(0);
        this.template1 = new MenuItem("Template1");
        this.template1.setUserData(1);
        this.template2 = new MenuItem("Template2");
        this.template2.setUserData(2);

        this.menuButton = new MenuButton("Select Template");
        this.menuButton.getItems().addAll(this.template0, this.template1, this.template2);

        super.getChildren().addAll(this.menuButton);
    }

    public MenuItem[] getChangeThemeMenuItems() {
        MenuItem[] menuItems = new MenuItem[3];
        menuItems[0] = this.template0;
        menuItems[1] = this.template1;
        menuItems[2] = this.template2;
        return menuItems;
    }
}
